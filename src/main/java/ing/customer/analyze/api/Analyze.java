package ing.customer.analyze.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ing.customer.analyze.domain.Customer;
import ing.customer.analyze.service.AnalyzeClassification;
import ing.customer.analyze.service.CustomerExtend;
import ing.customer.analyze.service.CustomerService;
import ing.customer.analyze.service.ScheduleAnalyzeJob;



@Path("/analyze")
public class Analyze {


	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Customer getUserById(@PathParam("id") Long id) {
		Customer u = CustomerService.getCustomerById(id);
		System.out.println(u.name);
		return u;
	}

	@GET
	@Path("{id}/{month}")
	@Produces(MediaType.APPLICATION_JSON)
	public CustomerExtend anaylzeUser(@PathParam("id") Long id, @PathParam("month") String month) {
		SimpleDateFormat formatter = new SimpleDateFormat("MM-yyyy");
		Date queryDate;
		try {
			queryDate = formatter.parse(month);
			CustomerExtend ce = AnalyzeClassification.analyze(id, CustomerService.firstDayOfMonth(queryDate));
			return ce;
		} catch (ParseException e) {
			e.printStackTrace();
			throw new WebApplicationException("Please inpu correct Date formate as 'MM-yyyy'.",
					Response.Status.BAD_REQUEST);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

	}

	@GET
	@Path("monthlyanalyze/{month}")
	@Produces(MediaType.APPLICATION_JSON)
	public String emailCustomerProfile(@PathParam("month") String month) {
		System.out.println("run montly analyze");
		//TODO: add analyze for special month
		AnalyzeClassification.analyze();
		return "success";
	}
	
	@GET
	@Path("start/monthlyreport")
	@Produces(MediaType.APPLICATION_JSON)
	public String startMonthlyAnalyze() {
		System.out.println("Start monthly customer analyze report job");
		ScheduleAnalyzeJob.startMonthlyAnalyze();
		return "success";
	}
	
	@GET
	@Path("stop/monthlyreport")
	@Produces(MediaType.APPLICATION_JSON)
	public String stopMonthlyAnalyze() {
		System.out.println("Stop monthly customer analyze report job");
		ScheduleAnalyzeJob.stopMonthlyAnalyze();
		return "success";
	}

}