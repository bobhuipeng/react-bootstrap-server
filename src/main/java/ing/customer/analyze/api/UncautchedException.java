package ing.customer.analyze.api;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
 
@Provider
public class UncautchedException  extends Throwable implements ExceptionMapper<Throwable> {
    private static final long serialVersionUID = 1L;
    
    @Override
    public Response toResponse(Throwable exception)
    {
        return Response.status(500).entity("Some errors happened. Please try late !!").type("text/plain").build();
    }
}
