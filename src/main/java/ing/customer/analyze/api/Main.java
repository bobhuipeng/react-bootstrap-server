package ing.customer.analyze.api;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import ing.customer.analyze.service.AnalyzeClassification;
import ing.customer.analyze.service.ScheduleAnalyzeJob;

import java.io.IOException;
import java.net.URI;

/**
 * Main class.
 *
 */
public class Main {
    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://localhost:8080/";

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        // create a resource config that scans for JAX-RS resources and providers
        // in ing.customer.analyze package
        final ResourceConfig rc = new ResourceConfig().packages("ing.customer.analyze");
        rc.register(new CORSFilter());
        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    /**
     * Main method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        final HttpServer server = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
    	//Start schedule job
    	ScheduleAnalyzeJob.startMonthlyAnalyze();
    	
    	//TODO this is only for show how this report work. remove it when presentation done.
    	 AnalyzeClassification.analyze();
    	 
        System.in.read();
        server.shutdownNow();
    }
}

