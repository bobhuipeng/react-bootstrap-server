package ing.customer.analyze.api;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ing.customer.analyze.domain.Customer;
import ing.customer.analyze.service.CustomerService;


@Path("/customer")
public class CustomerAPI {

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Customer getUserById(@PathParam("id") Long id) {
		Customer u = CustomerService.getCustomerById(id);
		return u;
	}

	@GET
	@Path("suggestedIds/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Long> getSuggestedUserIds(@PathParam("id") Long id) {
		List<Long> result = new ArrayList<Long>();
		try {
			result = CustomerService.getSuggestedIds(id);
		} catch (Exception ex) {
			System.err.println("Exception in get suggested user ids!");
			ex.printStackTrace();
		}
		return result;
	}

}