package ing.customer.analyze.service;

import java.util.Date;
import java.util.List;

import ing.customer.analyze.domain.Transaction;

public class TransactionService {
	
	public static List<Transaction> getAllTransactions(String username){
		
		return CustomerService.getCustomerByUsername(username).transactions;
	}
	
	public static List<Transaction> getAllTransactions(Long userId,Date start, Date end){
		
		return CustomerService.getCustomer(userId,start,end).transactions;
	}

}
