package ing.customer.analyze.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ing.customer.analyze.domain.Customer;

public class MokeData {



	public static List<Customer> getAllCustomers() {

        Gson gson = new GsonBuilder()
        		   .setDateFormat("MMM d, yyyy HH:mm:ss a").create();

        Customer[] customerList = gson.fromJson( readJson() ,  Customer[].class);  

       return Arrays.asList(customerList);
	}

	private static String readJson()
	{
		String content = "";

		try
		{
			content = new String ( Files.readAllBytes( Paths.get("./data.json") ) );
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return content;
	}


}
