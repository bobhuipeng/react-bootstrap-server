package ing.customer.analyze.service;

import java.util.ArrayList;

import ing.customer.analyze.domain.Customer;
import ing.customer.analyze.domain.Transaction;

public class CustomerExtend extends Customer {
	
	public Double currentBalance;
	public Integer totalTransaction; //count of transaction
	public Double totalSpend;
	public Double totalSave;
	public Transaction lastTransaction; //The last transaction of all the customer transactions.
	public Transaction customLastTransaction; //The last transaction of the search results. This is base on this.customer.transactions
//	public Customer customer;
	
	public CustomerExtend( Customer customer) {
		this.classification = new ArrayList<String>();
		this.id = customer.id;
		this.name = customer.name;
		this.transactions = customer.transactions;
	}
	
	public CustomerExtend() {
		this.classification = new ArrayList<String>();
		this.id = null;
		this.name = "";
		this.transactions =  new ArrayList<Transaction>();
		this.currentBalance = 0.0;
		this.totalTransaction = 0;
		this.totalSave =0.0;
		this.totalSpend = 0.0;
	}
	
}
