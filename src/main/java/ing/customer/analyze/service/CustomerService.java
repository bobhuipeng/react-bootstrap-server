package ing.customer.analyze.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import ing.customer.analyze.TransactionTypeEnum;
import ing.customer.analyze.domain.Customer;
import ing.customer.analyze.domain.Transaction;

public class CustomerService {

	public static Customer getCustomerByUsername(String username) {
		Customer result = null;
		List<Customer> allCustomer = MokeData.getAllCustomers();

		Optional<Customer> optional = allCustomer.stream().filter(cust -> cust.name.equals(username)).findFirst();

		if (optional.isPresent()) {// Check whether optional has element
			result = optional.get();// get it from optional

		}
		return result;
	}

	public static Customer getCustomerById(long userId) {
		Customer result = null;
		List<Customer> allCustomer = MokeData.getAllCustomers();

		Optional<Customer> optional = allCustomer.stream().filter(cust -> cust.id == userId).findFirst();

		if (optional.isPresent()) {// Check whether optional has element
			result = optional.get();// get it from optional

		}
		return result;
	}

	public static Customer getCustomerInMonth(Long userId, Date startDate) {
		return getCustomer(userId, startDate, nextMonth(startDate));
	}

	/**
	 * customer information and transaction between start date and end date. if the
	 * start date is null, then get all customer transaction before endDate if the
	 * end date is null, then get all the customer transaction after startDate if
	 * the startDate and endDate are both null, then get all user transactions.
	 * 
	 * @param username
	 *            customer username
	 * @param startDate
	 * @param endDate
	 * @return customer information and transaction between start date and end date.
	 */
	public static Customer getCustomer(Long userId, Date startDate, Date endDate) {

		Customer result = null;
		Customer customer = getCustomerById(userId); // get all customer information, and transaction
		if (customer != null) {

			List<Transaction> queryTansactions = customer.transactions.stream()
					.filter(trans -> (startDate == null ? true : trans.date.getTime() >= startDate.getTime())
							&& (endDate == null ? true : trans.date.getTime() < endDate.getTime()))
					.collect(Collectors.toList());
			queryTansactions.sort(Comparator.comparing(Transaction::getEpochDay));
			customer.transactions = queryTansactions;
			result = customer;
		}
		return result;
	}

	// public static Customer getCustomer(String username, Date startDate, Date
	// endDate) {
	//
	//// if (username == null || username.trim().equals("")) {
	//// return null;
	//// }
	//
	// Customer result = null;
	// Customer customer = getCustomerByUsername(username); // get all customer
	// information, and transaction
	// if (customer != null) {
	//
	// List<Transaction> oneMonthTrans = customer.transactions.stream()
	// .filter(trans -> (startDate == null ? true : trans.date.getTime() >=
	// startDate.getTime())
	// && (endDate == null ? true : trans.date.getTime() < endDate.getTime()))
	// .collect(Collectors.toList());
	// customer.transactions = oneMonthTrans;
	// result = customer;
	// }
	// return result;
	// }

	/**
	 * get last transaction of customer by the given username.
	 * 
	 * @param username
	 *            customer username
	 * @return last transaction
	 */
	public static Transaction getLastTransaction(String username) {
		Customer customer = getCustomerByUsername(username);
		return getLastTransactionInList(customer.transactions);
	}

	public static Transaction getLastTransactionWithEndDate(String username, Date endDate) {
		Customer customer = getCustomerByUsername(username);
		return getLastTransactionInList(customer.transactions);
	}

	/**
	 * get last transaction in the transaction list
	 * 
	 * if the transaction list input is NULL or empty, return NULL
	 * 
	 * @param transactions
	 *            {@link Transaction} list
	 * @return last transaction
	 */
	public static Transaction getLastTransactionInList(List<Transaction> transactions) {

		if (transactions != null && transactions.size() > 0) {
			Optional<Transaction> optionalTrans = transactions.stream()
					.max(Comparator.comparing(Transaction::getEpochDay));
			if (optionalTrans.isPresent()) {// Check whether optional has element
				return optionalTrans.get();// get it from optional
			}
		}

		return null;
	}

	public static Date nextMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, 1);
		return calendar.getTime();
	}

	public static Date lastMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		return calendar.getTime();
	}

	public static Date firstDayOfLastMonth() {
		return firstDayOfLastMonth(LocalDate.now());
	}

	public static Date firstDayOfLastMonth(Date date) {
		return firstDayOfLastMonth(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
	}

	public static Date firstDayOfLastMonth(LocalDate localDate) {
		return firstDayOfMonth(localDate.minusMonths(1));
	}

	public static Date firstDayOfMonth(Date date) {
		return firstDayOfMonth(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
	}

	public static Date firstDayOfMonth(LocalDate localDate) {
		LocalDate firstDayOfJuly = localDate.with(TemporalAdjusters.firstDayOfMonth());
		return Date.from(firstDayOfJuly.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	public static CustomerExtend getCustomerExtend(Long userId, Date startDate) {

		return getCustomerExtend(userId, startDate, nextMonth(startDate));
	}

	public static CustomerExtend getCustomerExtend(Long userId, Date startDate, Date endDate) {

		Customer customer = getCustomerInMonth(userId, startDate);

		if (customer == null) {// if no data match the query, return null
			return null;
		}

		CustomerExtend ce = new CustomerExtend(customer);
		
		
		ce.lastTransaction = getLastTransaction(ce.name);
		ce.currentBalance = getBalance(ce.lastTransaction);

		if (ce.transactions.size() == 0) {
			return ce;
		}
		
		ce.transactions.sort(Comparator.comparing(Transaction::getMinusEpochDay));
		ce.customLastTransaction = getLastTransactionInList(ce.transactions);
		ce.totalTransaction = ce.transactions.size();
		
		Double spend = 0.0;
		Double save = 0.0;

		for (Transaction trans : ce.transactions) {
			if (trans.type.equals(TransactionTypeEnum.CREDIT.getType())) {
				spend = save + trans.amount;
			} else if (trans.type.equals(TransactionTypeEnum.DEBIT.getType())) {
				spend = spend + trans.amount;
			}
		}

		ce.totalSpend = spend;
		ce.totalSave = save;

		

		return ce;
	}

	/**
	 * get the customer balance
	 * 
	 * @param lastTransaction
	 * @return
	 */
	public static double getBalance(Transaction lastTransaction) {
		return lastTransaction.balance
				+ lastTransaction.amount * (lastTransaction.type.equals(TransactionTypeEnum.CREDIT.getType()) ? 1 : -1);
	}

	public static double getSpend(List<Transaction> transactions) {
		return calculateAmount(transactions, TransactionTypeEnum.CREDIT.getType());
	}

	public static double getSave(List<Transaction> transactions) {
		return calculateAmount(transactions, TransactionTypeEnum.DEBIT.getType());
	}

	private static double calculateAmount(List<Transaction> transactions, String transactionType) {
		double n = 0.0;
		List<Transaction> creditList = transactions.stream().filter(trans -> trans.type.equals(transactionType))
				.collect(Collectors.toList());

		for (Transaction trans : creditList) {
			n = n + trans.amount;
		}
		return n;
	}

	public static List<Long> getSuggestedIds(Long id) {

		List<Long> suggestedIds = new ArrayList<Long>();
		String searchId = id.toString();
		MokeData.getAllCustomers().stream().map(cust -> cust.id).collect(Collectors.toList()).forEach(customerId -> {
			if (customerId.toString().startsWith(searchId)) {
				suggestedIds.add(customerId);
			}

		});
		return suggestedIds;
	}

	public static List<Long> getAllCustomerIds() {
		return MokeData.getAllCustomers().stream().map(cust -> cust.id).collect(Collectors.toList());
	}
}
