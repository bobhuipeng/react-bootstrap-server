package ing.customer.analyze.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import ing.customer.analyze.ClassificationEnum;
import ing.customer.analyze.TransactionTypeEnum;
import ing.customer.analyze.domain.Transaction;
import ing.customer.analyze.email.EmailService;

public class AnalyzeClassification {
	


	public static final List<String> POTENTIAL_LOAN_CLASSIFICATIONS = new ArrayList<String>() {
		{
			add(ClassificationEnum.BIG_SPENDER.getName());
			add(ClassificationEnum.FAST_SPENDER.getName());
		}
	};

	public static void analyze() {
		//TODO: use log instead of print
		System.out.println("Start customer analyze process!");
		Date firstDayOfLastMonth = CustomerService.firstDayOfLastMonth();
		List<CustomerExtend> results = new ArrayList<CustomerExtend>();
		for(long id:CustomerService.getAllCustomerIds()) {
			CustomerExtend ce = analyze(id, firstDayOfLastMonth);
			if(ce.classification.size()>0) {
				results.add(ce);
			}
		}
		
		EmailService.sendEmail(results);
		System.out.println("Customer analyze report Email has been sent!");
	}

	public static CustomerExtend analyze(long userId, Date startDate) {

		List<String> newList = new ArrayList<String>();

		// get user one month records
		CustomerExtend ce = CustomerService.getCustomerExtend(userId, startDate);

		if (ce == null || ce.transactions.size() < 1) {
			return ce;
		}

		if (isMorningPerson(ce)) {
			newList.add(ClassificationEnum.MORNING_PERSON.getName());
		} else if (isAfternoonPerson(ce)) {
			newList.add(ClassificationEnum.AFTERNOON_PERSON.getName());
		}

		// TODO: if the customer already has big spender or potential saver,
		// then only get the last month and check
		// in otherwise, get all transaction of the customer

		if (isBigSpender(ce, CustomerService.nextMonth(startDate))) {
			newList.add(ClassificationEnum.BIG_SPENDER.getName());
		} else if (isPotentialSaver(ce, CustomerService.nextMonth(startDate))) {
			newList.add(ClassificationEnum.POTENTIAL_SAVER.getName());
		}

		if (isbigTicketSpender(ce)) {
			newList.add(ClassificationEnum.BIG_TICKET_SPENDER.getName());
		}

		if (isFastSpender(ce)) {
			newList.add(ClassificationEnum.FAST_SPENDER.getName());
		}

		setPotentialLoan(newList); // TODO this need all months results

		ce.classification = newList;
		return ce;
	}

	public static boolean isAfternoonPerson(CustomerExtend customerEx) {
		return countTrancationBetweenDayTime(customerEx.transactions, 12, 24) > customerEx.totalTransaction / 2;

	}

	/**
	 * Spends over 80% of their deposits every month ($ value of deposits)
	 * 
	 * @param customerEx
	 * @param startDate
	 * @return
	 */
	public static boolean isBigSpender(CustomerExtend customerEx, Date endDate) {

		List<Transaction> transactions = TransactionService.getAllTransactions(customerEx.id, null, endDate);

		if (transactions.size() < 1) {
			return false;
		}

		Map<String, List<Transaction>> groupedTans = groupTansactionByMonth(transactions);

		for (Map.Entry<String, List<Transaction>> monthTrans : groupedTans.entrySet()) {

			if (!isOneMonthBigSpender(monthTrans.getValue())) {
				return false;
			}

		}
		return true;

	}

	private static boolean isOneMonthBigSpender(List<Transaction> oneMonthTransaction) {
		double deposit = getDeposits(oneMonthTransaction);
		double spend = CustomerService.getSpend(oneMonthTransaction);
		return spend / deposit > 0.8;
	}

	/**
	 * Makes one or more withdrawals over $1000 in the month
	 * 
	 * @param customerEx
	 */
	public static boolean isbigTicketSpender(CustomerExtend customerEx) {

		// TODO: set 1000 in config
		for (Transaction trans : customerEx.transactions) {
			if (trans.type.equals(TransactionTypeEnum.CREDIT.getType()) && trans.amount >= 1000) {
				return true;
			}
		}

		return false;

	}

	/**
	 * Spends over 75% of any deposit within 7 days of making it.
	 * 
	 * @param customerEx
	 * @return
	 */
	public static boolean isFastSpender(CustomerExtend customerEx) {

		customerEx.transactions.sort(Comparator.comparing(Transaction::getEpochDay));

		if (customerEx.transactions.size() > 7) {
			// check each 7 days range in the list.
			for (int n = 0; n < (customerEx.transactions.size() - 7); n++) {
				// if it is fast spender, then return true
				if (isSpendOver(customerEx.transactions.subList(n, n + 7), 0.75))
					return true;
			}

		} else {
			return isSpendOver(customerEx.transactions, 0.75);
		}

		return false;
	}

	private static boolean isSpendOver(List<Transaction> transactions, double percentage) {

		for (Transaction trans : transactions) {
			// any transaction spend more than given percentage
			if (TransactionTypeEnum.CREDIT.getType().equals(trans.type) && trans.amount / trans.balance > percentage)
				return true;
		}
		return false;
	}

	public static boolean isMorningPerson(CustomerExtend customerEx) {
		return countTrancationBetweenDayTime(customerEx.transactions, 0, 12) > customerEx.totalTransaction / 2;
	}

	/**
	 * Spends less than 25% of their deposits every month ($ value of deposits)
	 * 
	 * @param customerEx
	 * @param endDate
	 * @return
	 */
	public static boolean isPotentialSaver(CustomerExtend customerEx, Date endDate) {
		List<Transaction> transactions = TransactionService.getAllTransactions(customerEx.id, null, endDate);

		if (transactions.size() < 1) {
			return false;
		}

		Map<String, List<Transaction>> groupedTans = groupTansactionByMonth(transactions);

		for (Map.Entry<String, List<Transaction>> monthTrans : groupedTans.entrySet()) {
			if (!isOneMonthPotentialSaver(monthTrans.getValue())) {
				return false;
			}

		}
		return true;
	}

	private static boolean isOneMonthPotentialSaver(List<Transaction> oneMonthTransaction) {
		double deposit = getDeposits(oneMonthTransaction);
		double spend = CustomerService.getSpend(oneMonthTransaction);
		return spend / deposit < 0.25;
	}

	public static void setPotentialLoan(List<String> classifications) {

		if (classifications.containsAll(POTENTIAL_LOAN_CLASSIFICATIONS)) {
			classifications.removeAll(POTENTIAL_LOAN_CLASSIFICATIONS);
			classifications.add(ClassificationEnum.POTENTIAL_LOAN.getName());
		}

	}


	public static int countTrancationBetweenDayTime(List<Transaction> transactions, int startHour, int endHour) {
		int count = 0;

		for (Transaction tran : transactions) {
			if (tran.getLocalDateTime().getHour() >= startHour && tran.getLocalDateTime().getHour() < endHour) {
				count++;
			}
		}

		return count;
	}

	/**
	 * Deposit: it is the max balance of all transactions.
	 * 
	 * @param transactions
	 *            List of {@link Transaction}
	 * @return the max balance of all transactions
	 */
	public static double getDeposits(List<Transaction> transactions) {
		OptionalDouble optional = transactions.stream().mapToDouble(trans -> trans.balance).max();

		if (optional.isPresent()) {// Check whether optional has element
			return optional.getAsDouble();// get it from optional
		}
		return 0.0;
	}

	public static Map<String, List<Transaction>> groupTansactionByMonth(List<Transaction> transactions) {

		return transactions.stream().collect(Collectors.groupingBy(Transaction::getMonth));
	}

}
