package ing.customer.analyze.service;

import static java.util.concurrent.TimeUnit.DAYS;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

public class ScheduleAnalyzeJob {

	private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	private static ScheduledFuture<?> monthlyAnalyzeHandle;
	
	public static void startMonthlyAnalyze() {
		final Runnable analyze = new Runnable() {
			public void run() {
				LocalDate localDate = LocalDate.now();

			    LocalDate firstDayOfJuly = localDate.with(TemporalAdjusters.firstDayOfMonth());
			    
			    if(localDate.equals(firstDayOfJuly)) {
					//TODO: use log instead of print
					System.out.println("Start schedule job: Monthly Customer spending behaviors report.");
					AnalyzeClassification.analyze();
			    }
			}
		};
		//TODO use Quartz2 to do schedule
		monthlyAnalyzeHandle = scheduler.scheduleAtFixedRate(analyze, 1, 1, DAYS);
	}
	
	
	public static void stopMonthlyAnalyze() {
		monthlyAnalyzeHandle.cancel(true);
	}

}
