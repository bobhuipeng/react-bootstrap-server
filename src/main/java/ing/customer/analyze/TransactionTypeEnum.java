package ing.customer.analyze;

public enum TransactionTypeEnum {
	
	CREDIT("credit",1),
	DEBIT("debit",2);
	
	private String type;
	private int id;
	
	TransactionTypeEnum(String type,int id){
		this.type = type;
		this.id = id;
	}
	
	public String getType() {
		return this.type;
	}
	
	public int getId() {
		return this.id;
	}
}
