package ing.customer.analyze;

public enum ClassificationEnum {
	AFTERNOON_PERSON ("Afternoon Person",1),
	BIG_SPENDER ("Big Spender",2),
	BIG_TICKET_SPENDER ("Big Ticket Spender",3),
	FAST_SPENDER ("Fast Spender",4),
	MORNING_PERSON ("Morning Person",5),
	POTENTIAL_SAVER ("Potential Saver",6),
	POTENTIAL_LOAN ("Potential Loan",7);
	
	private String name;
	private int key;

	public String getName() {
		return name;
	}

	public int getKey() {
		return key;
	}
	ClassificationEnum(String name, int key) {
		this.name = name;
		this.key=key;
	}
	
}
