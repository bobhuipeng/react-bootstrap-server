package ing.customer.analyze.domain;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "transaction")
public class Transaction {
	public Long id;
	public Date date;
	public String message;
	public Double amount;
	public String type; // credit, debit
	public Double balance;
	public Boolean withdrawal;
	public LocalDateTime localDateTime = null;

	public long getEpochDay() {
		return this.date.getTime();
	}
	
	public long getMinusEpochDay() {
		return this.date.getTime()*-1;
	}

	public String getMonth() {

		LocalDateTime localDate = getLocalDateTime();
		return localDate.getYear() + "-" + localDate.getMonthValue();
	}
	

	public LocalDateTime getLocalDateTime() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(this.date);
		Instant instant = this.date.toInstant();
		ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
		this.localDateTime = zdt.toLocalDateTime();
		return this.localDateTime;
	}

}
