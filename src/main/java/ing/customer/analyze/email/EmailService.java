package ing.customer.analyze.email;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

import ing.customer.analyze.service.CustomerExtend;

public class EmailService {
	private static final String CUSTOMER_CLASSIFICATION_REPORT = "'%s' ING Customer Classification Report";

	public static String CUSTOMER_CLASSIFICATION_LINE = "<ul>%s : %s</ul>";

	public static void sendEmail(List<CustomerExtend> customers) {
		MailerService mailService = new MailerService();
		LocalDate date = LocalDate.now();
		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("d/MM/yyyy");

		List<String> ccEmailItems = new ArrayList<String>();
		ccEmailItems.add("bobhuipeng@gmail.com");
		String emailTo = "bobhuipeng@gmail.com";
		String bcc = emailTo;
		String emailFrom = "autoservice@ing.com.au";
		// set email body contents
		// TODO: add email contents
		Map<String, Object> contextItems = new HashMap<String, Object>();
		contextItems.put("departmentName", "Marketing Department");
		contextItems.put("customerList", customers);

		// TODO read from template
		StringBuffer emailTemplate = new StringBuffer(
				"<html><body><p>This is a monthly report for all customers spending habits.</p>");

		if (customers == null || customers.size() == 0) {
			emailTemplate.append("There is no transaction for any customer this month.");
		} else {
			emailTemplate.append("<ul>customer name : classifications</ul>");
			customers.forEach(ce -> {
				emailTemplate.append(
						String.format(CUSTOMER_CLASSIFICATION_LINE, ce.name, String.join(",", ce.classification)));
			});
		}
		emailTemplate.append("<p>Regards,<br>ING IT Support team</p>");
		emailTemplate.append("<p>This is an automated email from the ING analyze System. Please do not reply to this email.</p>");
		emailTemplate.append("</body></html>");
		// String subject, String from, String to, String cc, String bcc, String bodyHtm
		mailService.sendEmail(String.format(CUSTOMER_CLASSIFICATION_REPORT, date.format(formatters)), emailFrom,
				emailTo, StringUtils.join(ccEmailItems, ","), bcc, emailTemplate.toString());
	}
}
