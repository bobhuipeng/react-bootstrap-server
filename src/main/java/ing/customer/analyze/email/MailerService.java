package ing.customer.analyze.email;

import java.io.File;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.commons.lang3.StringUtils;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

public class MailerService {
	// Logger logger = LoggerFactory.getLogger(MailerService.class);
	private static Session session;

	// set default email to for DEV and TEST
	private static String defaultEmailTo;

	public MailerService() {
		if (session == null) {
			defaultEmailTo = "bob.dev.eamilservice@gmail.com";

			Properties props = new Properties();
			String smtpHost = "smtp.gmail.com";
			String smtpPort = "587";
			props.put("mail.smtp.host", smtpHost);
			props.put("mail.smtp.port", smtpPort);

			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", smtpHost);
			props.put("mail.smtp.port", smtpPort);
			// Add smtp host to trust to solve Exception : "Could not convert socket to TLS
			// GMail"
			props.put("mail.smtp.ssl.trust", smtpHost);
			props.put("mail.smtp.starttls.enable", "true");
			session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(defaultEmailTo, "kuiiidxvvvfklnud");
				}
			});
			;

		}
	}

	public void sendEmail(String subject, String to, String bodyHtml) {
		sendEmail(subject, null, to, null, null, bodyHtml, null);
	}

	public void sendEmail(String subject, String from, String to, String cc, String bcc, String bodyHtml) {
		sendEmail(subject, from, to, cc, bcc, bodyHtml, null);
	}

	public void sendEmail(String subject, String to, String bodyHtml, Map<String, String> attachments) {
		sendEmail(subject, to, null, null, bodyHtml, attachments);
	}

	public void sendEmail(String subject, String to, String cc, String bcc, String bodyHtml,
			Map<String, String> attachments) {
		sendEmail(subject, "", to, cc, bcc, bodyHtml, attachments);
	}

	public void sendEmail(String subject, String from, String to, String cc, String bcc, String bodyHtml,
			Map<String, String> attachments) {
		try {
			// use default email to for DEV and test

			if (!StringUtils.isBlank(defaultEmailTo)) {
				to = defaultEmailTo;
				cc = defaultEmailTo;
				bcc = defaultEmailTo;
			}

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));

			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			if (!StringUtils.isBlank(cc)) {
				message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
			}

			if (!StringUtils.isBlank(bcc)) {
				message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc));
			}

			message.setSubject(subject);

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();
			// Create a multipar message
			Multipart multipart = new MimeMultipart();

			messageBodyPart.setContent(bodyHtml, "text/html; charset=utf-8");

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			// attachment
			if (attachments != null) {
				attachments.forEach((_attachName, _attachFile) -> {
					try {
						BodyPart _messageBodyPart = new MimeBodyPart();
						File attachFile = new File(_attachFile);
						if (attachFile.exists()) {
							DataSource source = new FileDataSource(attachFile);
							_messageBodyPart.setDataHandler(new DataHandler(source));
							_messageBodyPart.setFileName(_attachFile);
							_messageBodyPart.setDescription(_attachName);
							multipart.addBodyPart(_messageBodyPart);
						} else {
							// logger.warn("Couldn't get attachment at '" + _attachFile + "'");
						}
					} catch (Exception e) {
						// logger.error("Email attachments Error!", e);
					}

				});
			}
			// Send the complete message parts
			message.setContent(multipart);
			message.saveChanges();

			Transport.send(message);

		} catch (MessagingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * read eamil template
	 * 
	 * @param templatePath
	 *            the template path with name
	 * @param contextItems
	 *            dynamic items in email context
	 * @return
	 */
	public String readEmailTemplate(String templatePath, Map<String, Object> contextItems) {
		/* first, get and initialize an engine */
		VelocityEngine ve = new VelocityEngine();
		StringWriter writer = new StringWriter();
		try {
			ve.init();
			/* next, get the Template */
			Template template = ve.getTemplate(templatePath);
			/* create a context and add data */
			VelocityContext context = new VelocityContext();
			if (contextItems != null) {
				contextItems.forEach((key, val) -> {
					context.put(key, val);
				});
			}
			template.merge(context, writer);
		} catch (Exception e) {
			e.printStackTrace();
			// logger.error(e.getMessage());
		}

		return writer.toString();
	}

}
