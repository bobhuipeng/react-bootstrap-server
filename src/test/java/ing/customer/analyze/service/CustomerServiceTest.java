package ing.customer.analyze.service;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.junit.Test;

import ing.customer.analyze.domain.Customer;
import ing.customer.analyze.domain.Transaction;

public class CustomerServiceTest {

	@Test
	public void testGetCustomer() {
		
		Customer customer =CustomerService.getCustomerByUsername("Anna");
		assertEquals(customer.name , "Anna");
		assertSame(customer.transactions.size(), 16);
		
		
		Customer nouser =CustomerService.getCustomerByUsername("No User");
		assertNull(nouser);
		
	}
	
	
	
	@Test
	public void testGetCustomerWithStartDate() {

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			Customer customer;
			customer = CustomerService.getCustomerInMonth(123456799L,formatter.parse( "01-06-2018"));
			assertEquals(customer.name , "Anna");
			assertSame(customer.transactions.size(), 4);
			
			customer = CustomerService.getCustomerInMonth(123456799L,formatter.parse( "01-12-2017"));
			assertEquals(customer.name , "Anna");
			assertSame(customer.transactions.size(), 8);
			
			
			customer = CustomerService.getCustomerInMonth(1239L,formatter.parse( "01-12-2017"));
			assertNull(customer);
			
			customer = CustomerService.getCustomerInMonth(123456799L,formatter.parse( "01-12-2015"));
			assertEquals("No record for particular Month",customer.transactions,new ArrayList<Transaction>());
		} catch (ParseException e) {
			e.printStackTrace();
			fail("Exception in date format");
		}
	}

}
