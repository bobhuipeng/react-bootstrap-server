package ing.customer.analyze.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

//import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

//import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import ing.customer.analyze.ClassificationEnum;
import ing.customer.analyze.domain.Customer;
import ing.customer.analyze.domain.Transaction;


public class AnalyzeClassificationTest {


	@Before
	public void setUp() throws Exception {

	}


	@Test
	public void testGroupTansactionByMonth() {
		try {

			Object anac = AnalyzeClassification.class.newInstance();// Class.forName("ing.customer.analyze.service.AnalyzeClassification").newInstance();
			Class<?>[] anacArg = new Class[1];
			anacArg[0] = List.class;
			Method m = anac.getClass().getDeclaredMethod("groupTansactionByMonth", anacArg);

			Customer customer;
			customer = CustomerService.getCustomerByUsername("Anna");

			Map<String, List<Transaction>> countMonths = (Map<String, List<Transaction>>) m.invoke(anac,
					customer.transactions);
			assertEquals(countMonths.size(), 3);

		} catch (Exception ex) {
			ex.printStackTrace();
			fail("Exception in reflection");
		}
	}

	@Test
	public void testCheckAfternoonPerson() {

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

			CustomerExtend ce = CustomerService.getCustomerExtend(123456799L, formatter.parse("01-12-2017"));
			boolean result = AnalyzeClassification.isAfternoonPerson(ce);
			assertTrue(result);

			//
			ce = CustomerService.getCustomerExtend(125856789L, formatter.parse("01-06-2018"));
			result = AnalyzeClassification.isAfternoonPerson(ce);
			assertFalse(result);
		} catch (ParseException e) {
			e.printStackTrace();
			fail("Exception in date format");
		}
	}
	
	@Test
	public void testCheckMorningPerson() {

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

			CustomerExtend ce = CustomerService.getCustomerExtend(123456799L, formatter.parse("01-12-2017"));
			boolean result = AnalyzeClassification.isMorningPerson(ce);
			assertFalse(result);

			//
			ce = CustomerService.getCustomerExtend(125856789L, formatter.parse("01-06-2018"));
			result = AnalyzeClassification.isMorningPerson(ce);
			assertTrue(result);
			
		} catch (ParseException e) {
			e.printStackTrace();
			fail("Exception in date format");
		}
	}
	
	
	@Test
	public void testCheckFarstSpender() {

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

			CustomerExtend ce = CustomerService.getCustomerExtend(125756789L, formatter.parse("01-06-2018"));
			boolean result = AnalyzeClassification.isFastSpender(ce);
			assertFalse(result);

			//
			ce = CustomerService.getCustomerExtend(123469889L, formatter.parse("01-06-2018"));
			result = AnalyzeClassification.isFastSpender(ce);
			assertTrue(result);
			
		} catch (ParseException e) {
			e.printStackTrace();
			fail("Exception in date format");
		}
	}
	
	
	@Test
	public void testPotentialSaver() {

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date startDate =formatter.parse("01-06-2018");
			CustomerExtend ce = CustomerService.getCustomerExtend(125756789L, startDate);
			boolean result = AnalyzeClassification.isPotentialSaver(ce,  CustomerService.nextMonth(startDate));
			assertTrue(result);

			ce = CustomerService.getCustomerExtend(123456659L, formatter.parse("01-06-2018"));
			result = AnalyzeClassification.isPotentialSaver(ce, CustomerService.nextMonth(startDate));
			assertFalse(result);
			
		} catch (ParseException e) {
			e.printStackTrace();
			fail("Exception in date format");
		}
	}
	
	
	
	@Test
	public void testBigTicketSpender() {

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date startDate =formatter.parse("01-06-2018");
			CustomerExtend ce = CustomerService.getCustomerExtend(123456659L, startDate);
			boolean result = AnalyzeClassification.isbigTicketSpender(ce);
			assertTrue(result);

			ce = CustomerService.getCustomerExtend(125856789L, formatter.parse("01-06-2018"));
			result = AnalyzeClassification.isbigTicketSpender(ce);
			assertFalse(result);
			
		} catch (ParseException e) {
			e.printStackTrace();
			fail("Exception in date format");
		}
	}
	
	
	@Test
	public void testBigSpender() {

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date startDate =formatter.parse("01-06-2018");
			CustomerExtend ce = CustomerService.getCustomerExtend(123456799L, startDate);
			boolean result = AnalyzeClassification.isBigSpender(ce,CustomerService.nextMonth(startDate));
			assertTrue(result);

			ce = CustomerService.getCustomerExtend(125856789L, formatter.parse("01-06-2018"));
			result = AnalyzeClassification.isBigSpender(ce,CustomerService.nextMonth(startDate));
			assertFalse(result);
			
		} catch (ParseException e) {
			e.printStackTrace();
			fail("Exception in date format");
		}
	}
	
	
	@Test
	public void testPotentialLoan() {

			List<String> classifications =new ArrayList<String>();
			classifications.add(ClassificationEnum.BIG_SPENDER.getName());
			AnalyzeClassification.setPotentialLoan(classifications);
			
			assertTrue(classifications.contains(ClassificationEnum.BIG_SPENDER.getName()));
			assertFalse(classifications.contains(ClassificationEnum.POTENTIAL_LOAN.getName()));
			
			classifications.add(ClassificationEnum.FAST_SPENDER.getName());
			AnalyzeClassification.setPotentialLoan(classifications);
			
			assertFalse(classifications.contains(ClassificationEnum.BIG_SPENDER.getName()));
			assertFalse(classifications.contains(ClassificationEnum.FAST_SPENDER.getName()));
			assertTrue(classifications.contains(ClassificationEnum.POTENTIAL_LOAN.getName()));
//
//	
//			assertFalse(result);
			
	}


}
